=============================
{{cookiecutter.project_name}}
=============================

{{ cookiecutter.project_short_description }}

Git
---

- Init your repo (first create it on-line and make note of the remote URI)::

    git init
    git add .
    git commit -m 'Cookiecutter stubs'
    git remote add origin MYREPOURI
    git push origin main

Then, you can choose between docker container (recommended) and virtualenv for
your development environment.


Docker
------

For more controlled deployments and to get rid of "works on my computer" -syndrome, we always
make sure our software works under docker.

It's also a quick way to get started with a standard development environment.

SSH agent forwarding
^^^^^^^^^^^^^^^^^^^^

We need buildkit_::

    export DOCKER_BUILDKIT=1

.. _buildkit: https://docs.docker.com/develop/develop-images/build_enhancements/

And also the exact way for forwarding agent to running instance is different on OSX::

    export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"

and Linux::

    export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"

Creating a development container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Build image, create container and start it::

    docker build --progress plain --ssh default --target devel_shell --build-arg GROUP_ID=$(id -g) --build-arg USER_ID=$(id -u) -t {{cookiecutter.package_name}}:devel_shell .
    docker create --name {{cookiecutter.package_name}}_devel -v `pwd`":/app" -it `echo $DOCKER_SSHAGENT` {{cookiecutter.package_name}}:devel_shell
    docker start -i {{cookiecutter.package_name}}_devel

Test suite
^^^^^^^^^^

You can use the devel shell to run py.test when doing development, for CI use
the "tox" target in the Dockerfile::

    docker build --progress plain --ssh default --target tox -t {{cookiecutter.package_name}}:tox .
    docker run --rm -it -v `pwd`":/app" `echo $DOCKER_SSHAGENT` {{cookiecutter.package_name}}:tox

Production docker
^^^^^^^^^^^^^^^^^

TODO: Remove this section if this is a library and not an application

There's a "production" target as well for running the application, remember to change that
architecture tag to arm64 if building on ARM::

    docker build --progress plain --ssh default --target production --build-arg GROUP_ID=$(id -g) --build-arg USER_ID=$(id -u) -t {{cookiecutter.package_name}}:latest .
    docker run -it --name {{cookiecutter.package_name}} {{cookiecutter.package_name}}:amd64-latest


virtualenv
----------

- Have python3.11 and virtualenvwrapper isntalled on your system.

- install Poetry: https://python-poetry.org/docs/#installation

- Create and activate a Python 3.11 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.11` my_virtualenv

- Install dependencies:

    poetry install

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).:

    workon my_virtualenv


Development
-----------

Whether you are in a container or in a virtualenv:

- change to a branch::

    git checkout -b my_branch

- Install pre-commit hooks::

    pre-commit install
    pre-commit run --all-files

- Ready to go.
