#!/usr/bin/env python
import subprocess
import tempfile
import os


def ensure_trailing_newline(file_path):
    """Ensure the file ends with a trailing newline."""
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
        if not content.endswith('\n\n'):
            with open(file_path, 'a', encoding='utf-8') as writable_file:
                writable_file.write('\n')

def process_files_in_directory(root_dir):
    """Recursively process Python files in a directory."""
    for dirpath, _, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.endswith('.py'):  # Only process Python files
                print(f"Processing {os.path.join(dirpath, filename)}")
                ensure_trailing_newline(os.path.join(dirpath, filename))


# Cookiecutter removes trailing newlines from templated files.
# This causes tests to fail because python linters complain about it.
# To work around this, we ensure a trailing newline to all .py files.
process_files_in_directory(os.getcwd())


with tempfile.TemporaryDirectory() as tmpdirname:
    print("Attempting to run 'poetry lock'")
    subprocess.run(f"POETRY_VIRTUALENVS_PATH={tmpdirname} poetry lock", shell=True, check=False, capture_output=False, encoding='utf-8')

print("Attempting to run 'git init && git add .'")
subprocess.run('git init --initial-branch=main && git add .', shell=True, check=False, capture_output=False, encoding='utf-8')

print("""
***********************************************
Created as {{ cookiecutter.repo_name }}

Check README.rst for further instructions

Especially the "Development" section at the end
***********************************************
""")
