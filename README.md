# datascience-template

Cookiecutter template for new Python 3.11 Data Science projects.

Sets up the basics for quality code with pylint, mypy, pytest with coverage, and pre-commit framework with a set of hooks.

Sets up a directory structure designed for data science inspired by
https://drivendata.github.io/cookiecutter-data-science/#directory-structure and the
[Data Science in Production podcast](https://www.advancinganalytics.co.uk/blog/data-science-in-production-episode-5).

## Usage

  1. Install [cookiecutter](https://pypi.org/project/cookiecutter/)
  2. `cookiecutter gl:advian-oss/python-datascience_template`
  3. Fill in the forms
  4. Profit!

The newly generated directory structure has more information, remember init your repo first.

    cd python-mypackage
    git init
    git add .
    git commit -m 'Cookiecutter stubs'
