#!/bin/bash -x
BUILD_GID=1001
BUILD_UID=1001
test -t 1 && USE_TTY="-it"
set -e
old_workdir=$(pwd)
tmp_dir=$(mktemp -d "/tmp/ccdstestbuild.XXXXXXXX")
cd $tmp_dir
cookiecutter -f --no-input $old_workdir
cd python-nameless

# Init repo so it looks like a real project
git init
git checkout -b cctest
poetry lock
git add .
# git commit -am 'automated tests'

if [ $(uname -s ) == "Darwin" ]; then
  export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"
else
  export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"
  chmod a+rw -R .
fi

# We need buildkit
export DOCKER_BUILDKIT=1
# basic tests under docker
docker build --progress plain --ssh default --target test --build-arg GROUP_ID=$BUILD_GID --build-arg USER_ID=$BUILD_UID -t ccdstestbuild:test .
docker run $USE_TTY --rm `echo $DOCKER_SSHAGENT` ccdstestbuild:test
# Docker devel shell
docker build --progress plain --ssh default --target devel_shell --build-arg GROUP_ID=$BUILD_GID --build-arg USER_ID=$BUILD_UID -t ccdstestbuild:devel_shell .
docker run $USE_TTY --rm -v `pwd`:/app ccdstestbuild:devel_shell -c "source /root/.zshrc && SKIP=check-executables-have-shebangs pre-commit run --all-files"
docker run $USE_TTY --rm -v `pwd`:/app ccdstestbuild:devel_shell -c "source /root/.zshrc && which git-up"
# Docker production image
docker build --progress plain --ssh default --target production --build-arg GROUP_ID=$BUILD_GID --build-arg USER_ID=$BUILD_UID -t ccdstestbuild:production .
docker run $USE_TTY --rm ccdstestbuild:production true
# Tox tests under docker
docker build --progress plain --ssh default --target tox -t ccdstestbuild:tox .
docker run $USE_TTY --rm `echo $DOCKER_SSHAGENT` ccdstestbuild:tox

rm -rf $tmp_dir
